## Contenedor multietapa para Gitea, plataforma de gestión de repositorios Git
## Se basa en Debian y el objetivo es hacerla funcionar en Arm32v5
#
## Tomadas referencias desde la imagen de 0rzech:
## https://github.com/0rzech/gitea-docker

# Primera etapa: constructor de Gitea
FROM rgomeza/arm32v5-debian-s6-base as constructor

## Adaptado de los contenedores de Linuxserver.io
ARG FECHA_CONST
ARG VERSION
LABEL build_version="rgomeza version: ${VERSION}, Fecha de construcción: ${FECHA_CONST}"
LABEL maintainer "Rubén Gómez Antolí <rgomeza@mucharuina.com>"

ENV DEBIAN_FRONTED noninteractive
ENV DEBCONF_NOWARNING yes

## Versión de Gitea y repositorio
ARG GITEA_VERSION
ARG GITEA_CHECKSUM
ENV GITEA_REPO_URL="https://github.com/go-gitea/gitea.git"

## Definición de directorios

ENV GOPATH="/go"
ENV SRC_DIR="${GOPATH}/src/code.gitea.io/gitea"
WORKDIR "$SRC_DIR"

## Etiquetas de compilación
ARG GITEA_BUILD_TAGS="bindata sqlite"

RUN \
   echo "Instalando paquetes necesarios para construir Gitea" && \
   echo "Se añade el repo backport, en Stretch go < 1.8" && \
   echo "deb http://ftp.debian.org/debian stretch-backports main" \
     >> /etc/apt/sources.list && \
   apt-get update && \
   apt-get -y --no-install-recommends install gcc git make golang-go \
	go-bindata libc6-dev && \
  git clone --branch "v${GITEA_VERSION}" --depth 1 --no-checkout "$GITEA_REPO_URL" .; \
	git checkout "$GITEA_CHECKSUM" && \
  apt-get -y --no-install-recommends install -t stretch-backports golang-go \
       go-bindata && \
  echo "Compilando Gitea" && \
  TAGS="$GITEA_BUILD_TAGS" make generate build
#
# Segunda etapa: contenedor de producción
FROM rgomeza/arm32v5-debian-s6-base as produccion
#

# Variables de entorno

ENV USUARIO="git"
ENV GRUPO="git"

RUN \
   echo "Instalando paquetes necesarios para ejecutar Gitea" && \
   apt update && \
   apt -y --no-install-recommends install ca-certificates curl git \
	libpam-runtime openssh-server sqlite3 tzdata busybox-syslogd sudo && \
   echo "Creando directorio para el ejecutable sshd" && \
   mkdir -p /run/sshd && \
   echo "Añadiendo grupo y usuario usuario" && \
   addgroup --system ${GRUPO} && \
   adduser --system --no-create-home --disabled-password --home /data/git \
         --shell /bin/bash --ingroup ${GRUPO}  ${USUARIO}
#
COPY --from=constructor "/go/src/code.gitea.io/gitea/gitea" "/usr/local/bin/gitea"
COPY files /
#
VOLUME ["/data"]
#
ENV GITEA_CUSTOM="/data/gitea"
#
ENTRYPOINT ["/usr/local/bin/entrypoint"]
#
EXPOSE 22 3000
