#!/bin/sh

set -eu

gitea_version='1.8.0'
gitea_checksum='799f5e05c9af07507e9ac2fb5b814b255bc633a7'
image_name='rgomeza/arm32v5-gitea'
fecha="$(date +%Y%M%d)"
version=0.1

docker build \
  --build-arg GITEA_VERSION="$gitea_version" \
  --build-arg GITEA_CHECKSUM="$gitea_checksum" \
  --build-arg FECHA_CONST="$fecha" \
  --build-arg VERSION="$version" \
  -t "${IMAGE_NAME:-${image_name}}" \
  .
#
# Si habilito esto me da fallo:
# --disable-content-trust=false \

